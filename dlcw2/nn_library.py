import os
import numpy as np

def create_folder(path):
    if not os.path.isdir(path):
        os.mkdir(path)

# Define network modules and model class

class Linear:
    def __init__(self, W, b):
        self.W = W
        self.b = b
    def forward_pass(self, x):
        return np.dot(x, self.W) + self.b
    def backward_pass(self, dL_dy, y):
        return np.dot(dL_dy, self.W.T)
    def w_gradient(self, dL_dy, x):
        return np.dot(x.T, dL_dy)
    def b_gradient(self, dL_dy):
        return np.mean(dL_dy, axis=0)
    def update_weights(self, dL_dy, x, learning_rate):
        self.W -= learning_rate * self.w_gradient(dL_dy, x)
        self.b -= learning_rate * self.b_gradient(dL_dy)
    def get_params(self):
        return [self.W, self.b]
    def set_params(self, params):
        self.W, self.b = tuple(params)

class ReLU:
    def forward_pass(self, x):
        output = np.maximum(x, 0)
        self.last_output = output
        return output
    def backward_pass(self, dL_dy, y):
        return dL_dy * (y > 0)

def softmax_forward_pass(x):
    return np.exp(x) / np.sum(np.exp(x), axis=1, keepdims=True)

# class Softmax:
#     def forward_pass(self, x):
#         return softmax_forward_pass(x)
#     def backward_pass(self, dL_dy, y):
#         s = dL_dy * y
#         return s - np.sum(s)

# class CrossEntropy:
#     def forward_pass(self, p, x):
#         return -np.sum(p * np.log(x))
#     def backward_pass(self, p, x):
#         return - p/x

class CrossEntropyLogits:
    def forward_pass_predict(self, x):
        return softmax_forward_pass(x)
    def forward_pass(self, p, x):
        y = softmax_forward_pass(x)
        return y, np.mean(-np.sum(p * np.log(y), axis=1))
    def backward_pass(self, y, p):
        return y - p
    
class Model:
    def __init__(self, layers, name="unnamed"):
        self.layers = layers
        self.name = name

    def train_step(self, X, Y, learning_rate):
        # Forward pass
        layers_inp = [ X ]
        for i in range(len(self.layers) - 1):
            layers_inp += [ self.layers[i].forward_pass(layers_inp[i]) ]
        predictions, loss = self.layers[-1].forward_pass(Y, layers_inp[-1])
        # Backward pass
        layers_bp = [ self.layers[-1].backward_pass(predictions, Y) ]
        for i in range(len(self.layers) - 2):
            layer_i = len(self.layers) - 2 - i
            layers_bp += [ self.layers[layer_i].backward_pass(layers_bp[i], layers_inp[layer_i + 1]) ]
        layers_bp = layers_bp[::-1]
        # Update weights
        for i in range(len(self.layers) - 1):
            if getattr(self.layers[i], "update_weights", None):
                self.layers[i].update_weights(layers_bp[i], layers_inp[i], learning_rate)

    def predict(self, X):
        # Forward pass
        layers_inp = [ X ]
        for i in range(len(self.layers) - 1):
            layers_inp += [ self.layers[i].forward_pass(layers_inp[i]) ]
        predictions = self.layers[-1].forward_pass_predict(layers_inp[-1])
        return predictions

    def save(self):
        create_folder('models')
        create_folder('models/' + self.name)
        for i in range(len(self.layers)):
            if getattr(self.layers[i], "get_params", None):
                params = self.layers[i].get_params()
                for j in range(len(params)):
                    np.save('models/' + self.name + '/' + str(i) + "_" + str(j) + ".npy", params[j])

    def load(self):
        for i in range(len(self.layers)):
            if getattr(self.layers[i], "get_params", None):
                params = self.layers[i].get_params()
                new_params = []
                for j in range(len(params)):
                    new_params += [ np.load('models/' + self.name + '/' + str(i) + "_" + str(j) + ".npy") ]
                self.layers[i].set_params(new_params)


